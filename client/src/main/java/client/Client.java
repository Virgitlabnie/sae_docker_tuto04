package client;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Logger;

public class Client {
	public static void main(String[] args) {
		Logger log = Logger.getGlobal();

		String serverAddress = args[0];
		int serverPort = Integer.parseInt(args[1]);

		try {
			InetAddress ip = InetAddress.getLocalHost();
			String hostname = ip.getCanonicalHostName();
//			log.info("Client IP address: " + ip);
			System.out.println("Client IP address: " + ip);
//			log.info("Client Hostname: " + hostname);
			System.out.println("Client Hostname: " + hostname);
		} catch (UnknownHostException e) {
			log.severe(e.toString());
		}

		DatagramSocket socket = null;
		try {
			Scanner keyboard = new Scanner(System.in);
			String outMessage = "";
			while (!outMessage.contentEquals("bye")) {
				System.out.print("Your input for the up server: ");
				outMessage = keyboard.next();
				socket = new DatagramSocket();
//				log.info("send " + outMessage + " to " + serverAddress + ":" + serverPort);
				byte[] buffer = outMessage.getBytes();
				DatagramPacket segmentOut = new DatagramPacket(buffer, buffer.length,
						InetAddress.getByName(serverAddress), serverPort);
				socket.send(segmentOut);

				DatagramPacket segmentIn = new DatagramPacket(new byte[1024], 1024);
				socket.receive(segmentIn);
				String inMessage = new String(segmentIn.getData(), segmentIn.getOffset(), segmentIn.getLength());
				InetAddress serverHost = segmentIn.getAddress();
				serverPort = segmentIn.getPort();
//				log.info("received : " + inMessage + " from " + serverHost + ":" + serverPort);
				System.out.println("Server replied: " + inMessage);
			}
		} catch (Exception e) {
			log.severe(e.toString());
		} finally {
			socket.close();
		}

//		log.info("exiting Client main method");
	}
}
