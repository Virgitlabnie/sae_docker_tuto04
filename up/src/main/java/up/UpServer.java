package up;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

public class UpServer {

	public static void main(String[] args) {
		Logger log = Logger.getGlobal();
		log.info("Server coming up");

		int serverPort = Integer.parseInt(args[0]);

		try {
			InetAddress ip = InetAddress.getLocalHost();
			String hostname = ip.getCanonicalHostName();
			log.info("Server IP address: " + ip);
			log.info("Server Hostname: " + hostname);
		} catch (UnknownHostException e) {
			log.severe(e.toString());
		}

		DatagramSocket socket = null;
		try {
			log.info("start listening on UDP port " + serverPort);
			socket = new DatagramSocket(serverPort);
			while (true) {
				log.info("wait for request");
				byte[] buffer = new byte[9876];
				DatagramPacket segmentIn = new DatagramPacket(buffer, buffer.length);
				socket.receive(segmentIn);
				String inMessage = new String(segmentIn.getData(), segmentIn.getOffset(), segmentIn.getLength());
				InetAddress clientHost = segmentIn.getAddress();
				int clientPort = segmentIn.getPort();
				log.info("received : " + inMessage + " from " + clientHost + ":" + clientPort);

				// Send reply.
				String outMessage = inMessage.toUpperCase();
				buffer = outMessage.getBytes();
				DatagramPacket segmentOut = new DatagramPacket(buffer, buffer.length, clientHost, clientPort);
				socket.send(segmentOut);
				log.info("reply sent.");
			}
		} catch (Exception e) {
			log.severe(e.toString());
		} finally {
			socket.close();
		}

		log.info("Server exiting");
	}
}
